import pygame


class Block(pygame.sprite.Sprite):

    def __init__(self, color, x, y, block_width, block_height):
        super().__init__()
        self.image = pygame.Surface([block_width, block_height])
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
