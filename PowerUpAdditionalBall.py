import PowerUp
import Ball


class PowerUpAdditionalBall(PowerUp.PowerUp):
    def __init__(self, screen, color, x, y):
        super().__init__(screen, color, x, y)
        self.name = "Dodatkowa Piłka"

    def picking_up_effect(self, game):
        ball = Ball.Ball(game.white)
        game.balls.add(ball)
