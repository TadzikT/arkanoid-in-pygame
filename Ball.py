import pygame
import math
import random


class Ball(pygame.sprite.Sprite):
    # TODO: Dodać różne pozycje startowe piłki i kąt lotu. Ale żeby dalej przychodziła ukosem z boku ekranu (z obu
    # TODO: boków niech nadlatuje).
    def __init__(self, color):
        super().__init__()
        self.speed = 5.0
        self.x = 0 if random.randint(0, 1) == 1 else pygame.display.get_surface().get_width()
        self.y = random.uniform(180, 250)
        self.direction = random.randint(120, 170)
        self.width = 10
        self.height = 10
        self.image = pygame.Surface([self.width, self.height])
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.screenHeight = pygame.display.get_surface().get_height()
        self.screenWidth = pygame.display.get_surface().get_width()

    def bounce(self, diff):
        self.direction = (180 - self.direction) % 360
        self.direction -= diff

    def update(self):
        direction_radians = math.radians(self.direction)
        self.x += self.speed * math.sin(direction_radians)
        self.y -= self.speed * math.cos(direction_radians)
        self.rect.x = self.x
        self.rect.y = self.y
        if self.y <= 0:
            self.bounce(0)
            self.y = 1
        if self.x <= 0:
            self.direction = (360 - self.direction) % 360
            self.x = 1
            # TODO: Bug: za każdym spawnem piłki wykonuje sie ten if, lub poniższy if, bo piłki spawnują się na krawędzi
            # TODO: ekranu. Ale dzięki temu też, przy losowaniu wartości direction nie trzeba brać pod uwagę tego, że
            # TODO: piłki spawnują się po obu stronach - zostanie to skorygowane przez te ify tutaj, sprawdzające
            # TODO: kolizje.
        if self.x > self.screenWidth - self.width:
            self.direction = (360 - self.direction) % 360
            self.x = self.screenWidth - self.width - 1
        if self.y > self.screenHeight:
            return False
        else:
            return True
