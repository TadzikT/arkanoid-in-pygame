import pygame
import sys
import random
import Block
import Ball
import Player
from PowerUpAdditionalBall import PowerUpAdditionalBall

pygame.init()

# --------------------------------------SETTINGS--------------------------------------
blockWidth = 23
blockHeight = 15

blocksXCount = 32
blocksYCount = 5

screen = pygame.display.set_mode([800, 600])

pygame.display.set_caption('Arkanoid')

pygame.mouse.set_visible(1)

font = pygame.font.Font("font.ttf", 26)

# Co który zniszczony klocek średnio dropi PowerUp (jeśli 4, to co czwarty średnio klocek wydropi PowerUpa).
chanceForPowerUp = 1
# -----------------------------------END OF SETTINGS----------------------------------

background = pygame.Surface(screen.get_size())

clock = pygame.time.Clock()


class Game:
    def __init__(self):
        # TODO: Jakąś sensowną kolejność operacji w tym konstruktorze, żeby było czytelniej?
        self.black = (0, 0, 0)
        self.white = (255, 255, 255)
        self.blue = (0, 0, 255)
        self.yellow = (255, 255, 0)
        self.blocks = pygame.sprite.Group()
        self.balls = pygame.sprite.Group()
        self.powerUps = pygame.sprite.Group()
        self.other = pygame.sprite.Group()
        self.player = Player.Player(self.white)
        self.other.add(self.player)
        ball = Ball.Ball(self.white)
        self.balls.add(ball)
        self.score = 0
        self.topYOfAllBlocks = 80
        self.gameOver = False
        self.create_blocks()

    def create_blocks(self):
        for row in range(blocksYCount):
            for column in range(0, blocksXCount):
                block = Block.Block(self.blue, column * (blockWidth + 2) + 1, self.topYOfAllBlocks, blockWidth,
                                    blockHeight)
                self.blocks.add(block)
            self.topYOfAllBlocks += blockHeight + 2

    def game_loop(self):
        exit_program = False
        while not exit_program:
            # Limit do 60 fpsów.
            # TODO: napraw microstuttering - najprawdopoboniej powodowany przez niedokładny timer.
            clock.tick(60)
            # Wyczyść ekran, żeby móc na rysować kolejną klatkę.
            screen.fill(self.black)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    # TODO: Czemu przy zamykaniu gry krzyżykiem, przez sekundę jeszcze gra zostaje? Co robie źle?
                    return False
                # Gdy gra jest zakończona i gracz nacisnął lewy przycisk myszki, wyjdź z tej funkcji i zacznij nowy
                # obiekt gry (zresetuj grę).
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1 and self.gameOver:
                        return True
            self.player.update()
            for power_up in self.powerUps:
                if not power_up.update():
                    self.powerUps.remove(power_up)
            for ball in self.balls:
                if not ball.update():
                    self.balls.remove(ball)
            if len(self.balls) == 0 and len(self.powerUps) == 0:
                self.gameOver = True
            if self.gameOver:
                # "True" tutaj w argumencie włącza antialiasing dla fontu.
                text = font.render("Game Over", True, self.white)
                text_pos = text.get_rect(centerx=background.get_width() / 2, top=200)
                # blit() umieści napis na ekranie.
                screen.blit(text, text_pos)
                text = font.render("Naciśnij LPM, by zacząć od nowa.", True, self.white)
                text_pos = text.get_rect(centerx=background.get_width() / 2, top=250)
                screen.blit(text, text_pos)

            # TODO: Może używając timera (albo łatwiej: jakiś int trzymany w klasie Ball który ma np. wartość 5, i gdy
            # TODO: zostanie uderzona piłka o paletkę to przez 5 kolejnych klatek już nie sprawdzane jest odbicie tej
            # TODO: piłki z paletką. Zauważ że z krawędziami ekranu i z klockami piłki się nie bugują) można wyłączyć
            # TODO: tego ifa na krótki czas po tym jak jest kolizja piłki z paletką. Te rozwiązanie bez timera może
            # TODO: bugować jak piłka szybko odbije się od ściany i znowu od paletki, więc może odbicie od ścian będzie
            # TODO: resetować tego inta do 0.
            # TODO: Może to by naprawiło ten błąd ze ślizganiem się piłki po paletce, i czasem nawet przypadkowym Game
            # TODO: Overem bo się bezsensownie wyślizguje w dół.
            paddled_balls = pygame.sprite.spritecollide(self.player, self.balls, False)
            for paddled_ball in paddled_balls:
                # Kieruje piłki na boku, w zależności od tego gdzie na paletce została odbita piłka.
                diff = (self.player.rect.x + self.player.width / 2) - (paddled_ball.rect.x + paddled_ball.width / 2)
                # Poprawia pozycje y piłki, jeśli została uderzona kantem paletki
                paddled_ball.rect.y = screen.get_height() - self.player.rect.height - paddled_ball.rect.height - 1
                paddled_ball.bounce(diff)
            for ball in self.balls:
                dead_blocks = pygame.sprite.spritecollide(ball, self.blocks, True)
                if len(dead_blocks) > 0:
                    ball.bounce(0)
                    if len(self.blocks) == 0:
                        self.gameOver = True
                for deadBlock in dead_blocks:
                    self.score += 1
                    # 1 \ chanceForPowerUp to szansa na wypadnięcie PowerUpa ze zniszczonego klocka.
                    if random.randint(1, chanceForPowerUp) == 1:
                        power_up = PowerUpAdditionalBall(screen, self.yellow, deadBlock.rect.x +
                                                         deadBlock.rect.width / 2, deadBlock.rect.y +
                                                         deadBlock.rect.height / 2)
                        self.powerUps.add(power_up)
            picked_power_ups = pygame.sprite.spritecollide(self.player, self.powerUps, True)
            for power_up in picked_power_ups:
                # TODO: Przy podniesieniu tego PowerUpa, nowa piłka pojawia się na jedną klatke w lewym górnym rogu
                # TODO: ekranu, i dopiero w kolejnej klatce jest ok. Debugger pokazuje, że prawidłowe wartości x i y
                # TODO: i reszta, jest aplikowana dopiero w kolejnej klatce podczas rysowania self.balls.draw(screen).
                power_up.picking_up_effect(self)
            text = font.render("Wynik: " + str(self.score), True, self.white)
            text_pos = text.get_rect(left=10, top=10)
            screen.blit(text, text_pos)
            # Renderowanko
            self.other.draw(screen)
            self.blocks.draw(screen)
            self.balls.draw(screen)
            self.powerUps.draw(screen)
            pygame.display.flip()

while True:
    game = Game()
    if not game.game_loop():
        break
pygame.quit()
sys.exit()
