import pygame


class Player(pygame.sprite.Sprite):
    def __init__(self, color):
        super().__init__()
        self.width = 75
        self.height = 15
        self.image = pygame.Surface([self.width, self.height])
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.screenHeight = pygame.display.get_surface().get_height()
        self.screenWidth = pygame.display.get_surface().get_width()
        self.rect.x = 0
        self.rect.y = self.screenHeight - self.height

    def update(self):
        pos = pygame.mouse.get_pos()
        # pos[0] to jest oś X myszy, pos[1] to oś Y
        self.rect.x = pos[0] - self.width / 2
        if self.rect.x > self.screenWidth - self.width:
            self.rect.x = self.screenWidth - self.width
        if self.rect.x < 0:
            self.rect.x = 0
