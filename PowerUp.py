import pygame
import random
pygame.init()
font = pygame.font.Font("font.ttf", 14)


class PowerUp(pygame.sprite.Sprite):
    # Podaje się środkowy punkt x i y do argumenta konstruktora.
    def __init__(self, screen, color, x, y):
        super().__init__()
        self.screen = screen
        self.color = color
        self.name = "nic nic nic test test"
        self.witdh = self.height = 15
        self.speed = random.uniform(1, 5)
        self.image = pygame.Surface([self.witdh, self.witdh])
        self.image.fill(self.color)
        self.rect = self.image.get_rect()
        self.rect.x = x - self.witdh / 2
        self.rect.y = y - self.height / 2
        # Te dwie deklaracje poniżej są dlatego, że mi IDE jęczy że nie powinno się deklarować class variables w
        # metodzie (update_text())
        self.text = {}
        self.text_pos = {}
        self.update_text()

    # Zwraca false, jeśli powerUp wyleciał pod ekran.
    def update(self):
        self.rect.y += self.speed
        if self.rect.y > pygame.display.get_surface().get_height():
            return False
        self.update_text()
        return True

    # Funkcja tylko dla użytku tutaj, wewnątrz klasy, w kontruktorze i w update()
    def update_text(self):
        self.text = font.render(self.name, True, self.color)
        self.text_pos = self.text.get_rect(centerx=self.rect.x+self.witdh/2, centery=self.rect.y-font.get_height())
        self.screen.blit(self.text, self.text_pos)

    def picking_up_effect(self, game):
        print("Tej klasy proszę nie instacjonować. To jest klasa bazowa dla reszty PowerUpów.")
